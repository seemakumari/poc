DROP TABLE IF EXISTS `failure_execution_info`;
CREATE TABLE `failure_execution_info` (
  `active` bit(1) DEFAULT NULL,
  `repeat_count` int(10) DEFAULT NULL,
  `job_class` varchar(255) DEFAULT NULL,
  `job_group` varchar(255) DEFAULT NULL,
  `job_name` varchar(255) NOT NULL,
  `repeat_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`job_name`)
) ENGINE=INNODB;

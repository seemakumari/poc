package com.quartz.demo.component;

import com.quartz.demo.enitiy.FailureExecutionInfo;
import com.quartz.demo.jobs.CommunicationJob;
import com.quartz.demo.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SchedulerStartUpHandler implements ApplicationRunner {

    private final SchedulerService schedulerService;

    @Autowired
    public SchedulerStartUpHandler(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            log.info("Schedule all new scheduler jobs at app startup - starting");
            schedulerService.startAllSchedulers();
            log.info("Schedule all new scheduler jobs at app startup - complete");

            log.info("Schedule a new scheduler jobs for hit the simple service at app startup - starting");
            FailureExecutionInfo failureExecutionInfo = new FailureExecutionInfo();
            failureExecutionInfo.setJobClass(CommunicationJob.class.getName());
            failureExecutionInfo.setJobGroup("Communication");
            failureExecutionInfo.setJobName("connect");
            failureExecutionInfo.setActive(Boolean.TRUE);
            failureExecutionInfo.setRepeatTime(300000l);
            schedulerService.scheduleNewJob(failureExecutionInfo);

        } catch (Exception ex) {
            log.error("Schedule all new scheduler jobs at app startup - error", ex);
        }
    }
}

package com.quartz.demo.enitiy;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(catalog = "quartz_demo_db", name = "failure_execution_info")
@Data
public class FailureExecutionInfo {

    @Id
    private String jobName;

    private String jobGroup;

    private String jobClass;

    private Long repeatTime;

    private Boolean active;

    private Integer repeatCount = 0;
}

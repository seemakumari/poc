package com.quartz.demo.repository;

import com.quartz.demo.enitiy.FailureExecutionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FailureExecutionSchedulerRepository extends JpaRepository<FailureExecutionInfo, Long> {

    FailureExecutionInfo findByJobName(String jobName);
}

package com.quartz.demo.jobs;

import com.quartz.demo.enitiy.FailureExecutionInfo;
import com.quartz.demo.service.SchedulerService;
import com.quartz.demo.service.SimpleService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CommunicationJob extends QuartzJobBean {

    private final SimpleService service;
    private final SchedulerService schedulerService;

    @Autowired
    public CommunicationJob(SimpleService service, SchedulerService schedulerService) {
        this.service = service;
        this.schedulerService = schedulerService;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobKey jobKey = jobExecutionContext.getJobDetail().getKey();
        log.info("Executing Job with key {}", jobKey);
        Integer status = service.callConnectService(jobKey.toString());
        if(status != HttpStatus.OK.value()) {

            log.error("failed to connect with simple service");
            log.debug("creating job for reconnect with simple service");
            FailureExecutionInfo failureExecutionInfo = new FailureExecutionInfo();
            failureExecutionInfo.setJobClass(CommunicationJob.class.getName());
            failureExecutionInfo.setJobGroup("Communication_Retry");
            failureExecutionInfo.setJobName("reconnect");
            failureExecutionInfo.setActive(Boolean.TRUE);
            failureExecutionInfo.setRepeatTime(10000l);
            schedulerService.scheduleNewJob(failureExecutionInfo);
        } else {
            FailureExecutionInfo failureExecutionInfo = new FailureExecutionInfo();
            failureExecutionInfo.setJobClass(CommunicationJob.class.getName());
            failureExecutionInfo.setJobGroup(jobKey.getGroup());
            failureExecutionInfo.setJobName(jobKey.getName());
            schedulerService.unScheduleJob(failureExecutionInfo);
        }
    }
}

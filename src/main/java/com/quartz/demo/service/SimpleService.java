package com.quartz.demo.service;

import org.springframework.stereotype.Service;

public interface SimpleService {
    Integer callConnectService(String note);
}

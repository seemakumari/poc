package com.quartz.demo.service.impl;

import com.quartz.demo.service.SimpleService;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SimpleServiceImpl implements SimpleService {

    @Value(value = "${connect.service.url}")
    private String connectServiceUrl;

    private final OkHttpClient okHttpClient;

    @Autowired
    public SimpleServiceImpl(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    @Override
    public Integer callConnectService(String note) {
        Request request = new Request.Builder()
                .url(connectServiceUrl + note )
                .build();

        Call call = okHttpClient.newCall(request);
        Response response;
        try {
            response = call.execute();
            return response.code();
        } catch (IOException e) {
            return HttpStatus.SERVICE_UNAVAILABLE.value();
        }
    }
}

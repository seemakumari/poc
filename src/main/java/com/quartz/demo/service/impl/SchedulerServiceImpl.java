package com.quartz.demo.service.impl;

import com.quartz.demo.component.JobScheduleCreator;
import com.quartz.demo.enitiy.FailureExecutionInfo;
import com.quartz.demo.repository.FailureExecutionSchedulerRepository;
import com.quartz.demo.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Slf4j
@Transactional
@Service
public class SchedulerServiceImpl implements SchedulerService {

    private final SchedulerFactoryBean schedulerFactoryBean;

    private final FailureExecutionSchedulerRepository failureExecutionSchedulerRepository;

    private final ApplicationContext context;

    private final JobScheduleCreator scheduleCreator;

    @Autowired
    public SchedulerServiceImpl(SchedulerFactoryBean schedulerFactoryBean, FailureExecutionSchedulerRepository failureExecutionSchedulerRepository, ApplicationContext context, JobScheduleCreator scheduleCreator) {
        this.schedulerFactoryBean = schedulerFactoryBean;
        this.failureExecutionSchedulerRepository = failureExecutionSchedulerRepository;
        this.context = context;
        this.scheduleCreator = scheduleCreator;
    }

    @Override
    public void startAllSchedulers() {
        List<FailureExecutionInfo> jobInfoList = failureExecutionSchedulerRepository.findAll();
        if (jobInfoList != null) {
            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            jobInfoList.forEach(jobInfo -> {
                try {
                    JobDetail jobDetail = JobBuilder.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
                            .withIdentity(jobInfo.getJobName(), jobInfo.getJobGroup()).build();
                    if (!scheduler.checkExists(jobDetail.getKey()) && jobInfo.getActive()) {
                        jobDetail = scheduleCreator.createJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()),
                                false, context, jobInfo.getJobName(), jobInfo.getJobGroup());

                        Trigger trigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(),
                                    jobInfo.getRepeatTime(), SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

                        scheduler.scheduleJob(jobDetail, trigger);

                    }
                } catch (ClassNotFoundException e) {
                    log.error("Class Not Found - {}", jobInfo.getJobClass(), e);
                } catch (SchedulerException e) {
                    log.error(e.getMessage(), e);
                }
            });
        }
    }

    @Override
    public void scheduleNewJob(FailureExecutionInfo jobInfo) {
        try {
            Scheduler scheduler = schedulerFactoryBean.getScheduler();

            JobDetail jobDetail = JobBuilder.newJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()))
                    .withIdentity(jobInfo.getJobName(), jobInfo.getJobGroup()).build();

            if (!scheduler.checkExists(jobDetail.getKey())) {
                jobDetail = scheduleCreator.createJob((Class<? extends QuartzJobBean>) Class.forName(jobInfo.getJobClass()),
                        false, context, jobInfo.getJobName(), jobInfo.getJobGroup());

                Trigger trigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(), jobInfo.getRepeatTime(),
                            SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

                scheduler.scheduleJob(jobDetail, trigger);
                failureExecutionSchedulerRepository.save(jobInfo);
            } else {
                log.error("scheduleNewJobRequest:  job Already Exist");
                if(jobInfo.getJobGroup().equalsIgnoreCase("Communication_Retry")) this.updateScheduleJob(jobInfo);
            }
        } catch (ClassNotFoundException e) {
            log.error("Class Not Found - {}", jobInfo.getJobClass(), e);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    public FailureExecutionInfo updateCommunicationErrorJob(FailureExecutionInfo jobInfo) {
        FailureExecutionInfo failureExecutionInfo = failureExecutionSchedulerRepository.findByJobName(jobInfo.getJobName());
        if (Objects.nonNull(failureExecutionInfo)) {
            log.error("scheduleNewJobRequest:  job Already Exist");
            log.debug("reschedule job : " + jobInfo.getJobName());

            Integer newRepeatCount = failureExecutionInfo.getRepeatCount() + 1;
            long newRepeatTime = failureExecutionInfo.getRepeatTime() + jobInfo.getRepeatTime();

            jobInfo.setRepeatTime(newRepeatTime);
            jobInfo.setRepeatCount(newRepeatCount);
        }
        return jobInfo;
    }

    @Override
    public void updateScheduleJob(FailureExecutionInfo failureExecutionInfo) {
        FailureExecutionInfo jobInfo = this.updateCommunicationErrorJob(failureExecutionInfo);

        if (jobInfo.getRepeatCount() > 20) {
            if (this.unScheduleJob(jobInfo)) {
                log.debug("unSchedule Job : " + failureExecutionInfo.getJobName());
            }
            return;
        }

        Trigger newTrigger = scheduleCreator.createSimpleTrigger(jobInfo.getJobName(), new Date(), jobInfo.getRepeatTime(),
                    SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
        try {
            schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobInfo.getJobName()), newTrigger);
            failureExecutionSchedulerRepository.save(jobInfo);
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean unScheduleJob(FailureExecutionInfo jobInfo) {
        try {
            FailureExecutionInfo failureExecutionInfo = failureExecutionSchedulerRepository.findByJobName(jobInfo.getJobName());
            failureExecutionInfo.setActive(Boolean.FALSE);
            failureExecutionSchedulerRepository.save(failureExecutionInfo);
            return schedulerFactoryBean.getScheduler().unscheduleJob(TriggerKey.triggerKey(jobInfo.getJobName()));
        } catch (SchedulerException e) {
            log.error("Failed to unSchedule job - {}", jobInfo.getJobName(), e);
            return false;
        }
    }

}

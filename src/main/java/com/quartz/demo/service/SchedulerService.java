package com.quartz.demo.service;

import com.quartz.demo.enitiy.FailureExecutionInfo;

public interface SchedulerService {

    void startAllSchedulers();

    void scheduleNewJob(FailureExecutionInfo jobInfo);

    void updateScheduleJob(FailureExecutionInfo jobInfo);

    boolean unScheduleJob(FailureExecutionInfo jobInfo);

}
